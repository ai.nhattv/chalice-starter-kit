
Coding principle
==================================

1. Clean code will help the life better and no stress 😝
2. No one comment better than good name 😅
3. Testability make your more seniority 😭


REST API backed by Amazon DynamoDB
==================================

This template provides a REST API that's backed by an Amazon DynamoDB table.
This application is deployed using the AWS CDK.

For more information, see the `Deploying with the AWS CDK
<https://aws.github.io/chalice/tutorials/cdk.html>`__ tutorial.


Quickstart
----------

First, you'll need to install the AWS CDK if you haven't already.
The CDK requires Node.js and npm to run.
See the `Getting started with the AWS CDK
<https://docs.aws.amazon.com/cdk/latest/guide/getting_started.html>`__ for
more details.

``` bash
$ npm install -g aws-cdk
```

Next you'll need to install the requirements for the project.

``` bash
$ pip install -r requirements.txt
```

There's also separate requirements files in the ``infrastructure``
and ``runtime`` directories if you'd prefer to have separate virtual
environments for your CDK and Chalice app.

To deploy the application, ``cd`` to the ``infrastructure`` directory.
If this is you're first time using the CDK you'll need to bootstrap
your environment.

``` bash
$ cdk bootstrap
```

Then you can deploy your application using the CDK.

``` bash
$ cdk deploy
```

Project layout
--------------

This project template combines a CDK application and a Chalice application.
These correspond to the ``infrastructure`` and ``runtime`` directory
respectively.  To run any CDK CLI commands, ensure you're in the
``infrastructure`` directory, and to run any Chalice CLI commands ensure
you're in the ``runtime`` directory.
