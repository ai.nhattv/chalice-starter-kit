# Mission

Runtime folder aimed to implement logic according functions and test base functions

## 1. Setup local environment

! Ensure you have available development environment with `chalice`, if not please install development packages `${PWD}/requirements-dev.txt`

```bash
# install package dependencies
pip3 install -r requirements.txt 

# tests
pytest ./tests

# local start
chalice local
```


## 2. deployment steps
1. Ensure your local tests are passed
2. Deploy new version use aws cdk