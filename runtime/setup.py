import setuptools

with open("README.md", "r") as fh:
    long_description = fh.read()

setuptools.setup(
    name="chalicelib",
    version="1.0.0",
    author="nhat.tran",
    author_email="nhat.tran@vincere.io",
    description="<Template Setup.py package>",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="<https://github.com/authorname/templatepackage>",
    classifiers=["Programming Language :: Python :: 3",
                 "License :: OSI Approved :: MIT License",
                 "Operating System :: OS Independent",
                 ],
    python_requires='>=3.6',
    packages=['chalicelib']
)
