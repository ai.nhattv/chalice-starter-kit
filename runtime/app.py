from chalice import Chalice
import logging

from chalicelib.apis.todo.api import todo_api
from chalicelib.events.todo_handler import todo_handler
from chalicelib.middlewares.error_handler import handle_errors


app = Chalice(app_name='vc.chalice')
app.register_blueprint(todo_api, url_prefix='/api/v1/todos')
app.register_blueprint(todo_handler)
app.register_middleware(handle_errors)

app.log.setLevel(logging.DEBUG)


@app.route('/')
@app.route('/hc')
def health_check():
    return "I'm live"
