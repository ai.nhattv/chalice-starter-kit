from unittest import TestCase, mock
from chalice.test import Client

from app import app
from chalicelib.apis.todo import api as todo_api
from chalicelib.apis.todo.db import DynamoKeyProviderDB


class TestTodoApi(TestCase):

    def setUp(self):
        self.client = Client(app)
        self._register_mocking()

    def test_list_api_response_success(self):
        with self.mock_get_todos:
            # arrange
            name = "dev.io"

            # act
            response = self.client.http.get(f"/api/v1/todos/{name}")
            content = response.json_body

            # assert
            self.assertEqual(response.status_code, 200)
            self.assertEqual(content, [{
                "Name": "dev.io",
                "KeyId": "ABC123",
            }])

    def _register_mocking(self):
        db = DynamoKeyProviderDB()
        db.get_todos = mock.Mock(side_effect=[
            [{
                "Name": "dev.io",
                "KeyId": "ABC123",
            }]
        ])

        self.mock_get_todos = mock.patch.object(
            todo_api, '_get_todo_db', return_value=db
        )
