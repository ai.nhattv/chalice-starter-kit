from unittest import TestCase
from chalice.test import Client
from app import app


class TestHealthCheckApi(TestCase):

    def setUp(self):
        self.client = Client(app)

    def test_health_check_api_alway_available(self):
        response_0 = self.client.http.get("/")
        response_1 = self.client.http.get("/hc")

        self.assertEqual(response_0.status_code, 200)
        self.assertEqual(response_1.status_code, 200)
