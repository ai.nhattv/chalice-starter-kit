from unittest import TestCase
from chalice.test import Client
from app import app


class TestTodoEventHandler(TestCase):

    def setUp(self):
        self.client = Client(app)

    def test_todo_sns_handler(self):
        response = self.client.lambda_.invoke(
            "on_message_handler",
            self.client.events.generate_sns_event(message="hello world")
        )
        self.assertDictEqual(response.payload, {'message': 'hello world'})
