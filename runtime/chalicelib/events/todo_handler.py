from chalice import Blueprint

todo_handler = Blueprint(__name__)


@todo_handler.on_sns_message(topic='todo_topic')
def on_message_handler(event):
    return {'message': event.message}
