def handle_errors(event, get_response):
    try:
        return get_response(event)
    except Exception() as e:
        return {"Error": e.__class__.__name__,
                "Message": str(e)}
