class KeyProviderDB():
    """[Interface]
    """

    def get_todo(self, todo_name: str, todo_id: str) -> dict:
        pass

    def create_todo(self, todo_name: str, name: str) -> dict:
        pass

    def get_todos(self, todo_name: str) -> list:
        pass


class DynamoKeyProviderDB(KeyProviderDB):
    def __init__(self, *args):
        if len(args) > 1:
            self._table = args[0]

    def get_todo(self, todo_name: str, todo_id: str) -> dict:
        pass

    def create_todo(self, todo_name: str, name: str) -> dict:
        pass

    def get_todos(self, todo_name: str) -> list:
        pass
