import boto3
from chalice import Blueprint
from .db import DynamoKeyProviderDB
from chalicelib.settings import DYNAMO_TODO_TABLE

todo_api = Blueprint(__name__)

_TODO_TABLE = None


@todo_api.route('/{name}', methods=['GET'])
def get_todos(name: str):
    todo_api.log.info(f'getting todo {name}')

    todos = _get_todo_db().get_todos(name)

    return todos


def _get_todo_db() -> DynamoKeyProviderDB:
    global _TODO_TABLE

    if _TODO_TABLE is None:
        dynamodb = boto3.resource('dynamodb')
        dynamodb_table = dynamodb.Table(DYNAMO_TODO_TABLE)
        _TODO_TABLE = DynamoKeyProviderDB(dynamodb_table)

    return _TODO_TABLE
